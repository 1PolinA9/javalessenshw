package ru.shaveleva.hw1.task1;

import java.util.Scanner;
/**
 *    Вычеслить среднее арифметическое двух значений х1 и х2.
 */
public class App {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double x1 = scanner.nextDouble();
        double x2 = scanner.nextDouble();
        System.out.println((x1 + x2) / 2);
    }
}


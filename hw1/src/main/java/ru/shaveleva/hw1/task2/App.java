package ru.shaveleva.hw1.task2;

import java.util.Scanner;
/**
*   Перевести длину, заданную в дюймах, в сантиметры.
*/
public class App {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double d = scanner.nextDouble();
        System.out.println(d * 2.54);
    }
}

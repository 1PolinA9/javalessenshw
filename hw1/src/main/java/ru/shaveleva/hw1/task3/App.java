package ru.shaveleva.hw1.task3;

import java.util.Scanner;
/**
 *   Перевести длину, заданную в сантиметрах, в дюймы.
 */
public class App {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double s = scanner.nextDouble();
        System.out.println(s / 2.54);
    }
}

package ru.shaveleva.hw1.task4;

import java.util.Scanner;
/**
 *   Перевести температуру из шкалы Фарингейта в шкалу Цельсия.
 */
public class App {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double f = scanner.nextDouble();
        System.out.println((5.0 / 9.0)  * (f - 32.0));
    }
}

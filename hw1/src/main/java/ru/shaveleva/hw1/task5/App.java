package ru.shaveleva.hw1.task5;

import java.util.Scanner;
/**
 *   Перевести температуру из шкалы Цельсия в шкалу Фарингейта.
 */
public class App {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double c = scanner.nextDouble();
        System.out.println(((9.0 / 5.0) * c) + 32.0);
    }
}

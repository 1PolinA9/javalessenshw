package ru.shaveleva.hw2.task1;

import java.util.Scanner;

/**
 *      Проверить для четырехзначных чисел признак делимости на 11 – число делится на 11,
 *      если разность сумм цифр, стоящих на четных и нечетных позициях, делится на 11.
 */
public class App {
    public static void main(String[] args) {
        int number = readStr();
        boolean result = checkCondition(number);
        printResult(result, number);
    }

    private static int readStr() {
        Scanner scanner = new Scanner(System.in);
        do {
            int number;
            System.out.print("Введите 4-ех значное число: ");
            number = scanner.nextInt();
            if (number > 999 && number < 10000) {
                return number;
            } else {
                System.out.println("Число не корректно!");
            }
        } while (true);
    }

    private static boolean checkCondition(int number) {
        int a = (number / 1000) + (number / 10 % 10);
        int b = (number / 100 % 10) + (number % 10);
        return (a - b) == 0;
    }

    private static void printResult(boolean result, int number) {
        if (result) {
            System.out.println("Число " + number + " делится на 11 нацело");
        } else {
            System.out.println("Число " + number + " не делится на 11 нацело");
        }
    }
}

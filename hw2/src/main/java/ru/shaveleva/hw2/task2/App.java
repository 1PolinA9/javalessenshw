package ru.shaveleva.hw2.task2;

import java.util.Scanner;

/**
 *      Целое четырехзначное число заменить числом, получающимся при записи
 *      его цифр в обратном порядке (число всегда больше 0)
 */
public class App {
    public static void main(String[] args) {
        System.out.print("Введите 4-ех значное число: ");
        Scanner scanner = new Scanner(System.in);
        int s = scanner.nextInt();

        if (s > 999 && s < 10000) {
                System.out.println(s % 10 + "" + s / 10 % 10 + "" + s / 100 % 10 + "" + s / 1000);
        } else {
            System.out.println("Введите четырехзначное число");
        }
    }

}

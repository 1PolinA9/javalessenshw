package ru.shaveleva.hw2.task3;

import java.util.Scanner;

/**
 *      Ввести время в секундах, перевести в формат чч:мм:сс.
 */
public class App {
    public static void main(String[] args) {
        System.out.print("Введит время в секундах: ");
        Scanner scanner = new Scanner(System.in);

        long secEntered = scanner.nextLong();
        long second = (secEntered % 60);
        long minute = ((secEntered / 60) % 60);
        long hour = (secEntered / 3600);

        System.out.println(String.format("%d:%02d:%02d", hour, minute, second));
    }
}

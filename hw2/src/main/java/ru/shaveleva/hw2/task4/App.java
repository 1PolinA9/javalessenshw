package ru.shaveleva.hw2.task4;

import java.util.Scanner;

/**
 *      Ввести дату в формате дд:мм:гггг и вывести дату следующего дня.
 */

public class App {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Введите день: ");
        int day = scanner.nextInt();
        System.out.print("Введите месяц: ");
        int month = scanner.nextInt();
        System.out.print("Введите год: ");
        int year = scanner.nextInt();


        int nextDay = calculateNextDay(day, month, year);
        int nextMonth = calculateNextMonth(day, month, year);
        int nextYear = calculateNextYear(month, year);

        System.out.println("дата следующего дня: " + nextDay + ":" + nextMonth + ":" + nextYear);
    }
    private static int calculateNextDay(int day, int month, int year){
        int[] dayOfMonthArray = getDayOfMonthArray(year);
        if (day == dayOfMonthArray[month - 1]){
            return 1;
        } else {
            return day + 1;
        }

    }

    private static int calculateNextMonth(int day, int month, int year){
        int[] dayOfMonthArray = getDayOfMonthArray(year);
        if (day == dayOfMonthArray[month - 1]){
            if(month == 12){
                return 1;
            }else {
                return month + 1;
            }
        }
        return month;
    }

    private static int calculateNextYear(int month, int year){
        if (month == 12){
            return year + 1;
        }
        return year;
    }

    private static int[] getDayOfMonthArray(int year) {
        int[] dayOfMonthArray = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
        if ((year % 4 == 0) && (year % 400 == 0) && (year % 100 != 0)) {
            dayOfMonthArray[1] = 29;
        }
        return dayOfMonthArray;
    }
}


package ru.shaveleva.hw2.task5;

import java.util.Scanner;

/**
 *      Напечатать длину интервала между двумя моментами времени в пределах суток (все формате чч:мм:сс).
 */
public class App {
    private static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {

        System.out.println("Введите 1-ый момент времени.");
        Time time1 = readTimeMoment();

        System.out.println("Введите 2-ый момент времени.");
        Time time2 = readTimeMoment();

        int lengthOfMoment = time2.getTimeMomentInSec() - time1.getTimeMomentInSec();

        System.out.println("Разница между моментами: " + lengthOfMoment + " секунд(ы).");
    }

    private static Time readTimeMoment(){

        int hour = sc.nextInt();
        int minute = sc.nextInt();
        int second = sc.nextInt();
        return new Time(hour, minute, second);
    }
}

